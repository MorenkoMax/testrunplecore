<?php
namespace app\models\User;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;

/**
 * Password
 *
 * @property string $authKey
 * @property string $passwordHash
 * @property string $passwordResetToken
 * @property int $updated_at
 */
class Password extends Model
{
    public $password;

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['authKey','password'], 'required'],
            [['authKey','password'], 'string'],
            ['updated_at', 'integer'],
        ];
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }
}