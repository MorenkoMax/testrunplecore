<?php
namespace app\models\User;

use yii\base\Model;
use yii\behaviors\TimestampBehavior;

/**
 * User model
 *
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserEntity extends Model
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status ? true : false;
    }
}